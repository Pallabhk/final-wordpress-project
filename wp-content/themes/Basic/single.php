<?php get_header();?>

<div class="single-post">
    <div class="post-area">
   <?php if(have_post){
       while(have_posts()){
           the_post();?>
           <h2><?php the_title();?></h2>
           <?php the_post_thumbnail();?>
           <p><?php the_content();?></p>
      <?php }
   }else{
     echo 'No post Here';
   }
    ?>


    </div>
</div>
<?php get_footer()?>
