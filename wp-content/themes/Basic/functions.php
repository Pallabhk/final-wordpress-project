<?php
    function pallab_theme(){
      add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
        add_image_size('post-thumb',600,400,true);
        register_nav_menus(array(
            'header_menu'  =>'Main Menu',
            'footer_menu'  =>'Footer Menu',
            'primary_menu' =>'Primary Menu'
        ));
        add_theme_support('html5');
    }

add_action('after_setup_theme','pallab_theme');

    function pallab_css_js(){
        wp_enqueue_style('pallab-test-style',get_template_directory_uri().'/css/test.css',array('main-style'),'v1.25',all);
        wp_enqueue_style('main-style',get_stylesheet_uri());

        wp_enqueue_script('pallab-main-js',get_template_directory_uri().'/js/main.js',array('jquery'),'v1.25.25',true);
        wp_enqueue_script('jquery');
    }
add_action('wp_enqueue_scripts','pallab_css_js');

    function pallab_widgets(){
        register_sidebar(array(
            'name' => 'First Widget',
            'id'   => 'pallab_first_widget'
        ));
        register_sidebar(array(
            'name' => 'Second Widget',
            'id'   => 'pallab_second_widget'
        ));
    }
add_action('widgets_init','pallab_widgets');
    function pallab_custom_posts(){
    register_post_type('pallab_products',array(
        'labels' => array(
            'name'      => 'Pallab_products',
            'menu_name' => 'pallab_menu',
            'all_items' =>'pallab_all_product',
            'add_new  ' =>'Add new product',
            'add_new_item'=>'Add new pallab product'
        ),
        'public' => true,
        'supports'=>array(
           'title','editor','thumbnail','excerpt','revisions','custom-fields','page-attributes'
        )
    ));
    }
add_action('init','pallab_custom_posts');
?>