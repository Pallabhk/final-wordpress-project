<?php get_header();?>

<h2>This is our wellcome page</h2>


<?php wp_nav_menu(array(
    'theme_location'  => 'primary_menu'
));?>


<section class="search-from" style="border: 2px solid green">
   <div>
       <?php
            if( !dynamic_sidebar('pallab_first_widget')){
                echo 'No sidebar';
            }
       ?>
   </div>
    <?php get_search_form();
        if(!dynamic_sidebar('pallab_second_widget')){
            echo 'Side bar';
        }
    ?>
</section>
<section class="custom_post" style="border: 2px solid red">
    <div class="all_post">
        <?php
            $pallab_post= new WP_Query(array(
                'post_type' =>'pallab_products',
                'posts_per_page'=>5
            ));
            if($pallab_post-> have_posts()){
                while($pallab_post-> have_posts()){
                    $pallab_post-> the_post();
                    $phone=get_post_meta(get_the_ID(),'phone_number',true);
                    $email_id=get_post_meta(get_the_ID(),'email',true);
                    ?>
                    <div class="single_post">
                        <h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
                        <h3><?php echo $phone;?></h3>
                        <h3><?php echo $email_id;?></h3>
                    </div>

               <?php }
            }else{
                echo 'no Post';
            }
                wp_reset_postdata();
        ?>

    </div>
</section>
<?php get_footer()?>

