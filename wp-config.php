<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ebit_full');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7&k;v_Q&C5K/5.|A>( yG)th?XRp}v<`8Ob%BnC{l#w_U<^%5,O!w>_lT(KnF[NS');
define('SECURE_AUTH_KEY',  '|f2.9}Gn^`cT2r>NUW&!kF/_O9HzwtS*JdBW&ha2c{B$c,@J[&:pQMs2#l#WOPP ');
define('LOGGED_IN_KEY',    '!-]xQ2>FeI,,.@V`^jBc[jL5gH2BHuQ+C0Il5XdZzOwa[7$ow_vL}X9tC7NLeeMn');
define('NONCE_KEY',        '=LXh:Ej#jOW+l4dgFx-U>J/t[6C~iPT9<Atf]gpoVQy wZZAsgH@,kb9tD(,NdQ`');
define('AUTH_SALT',        'IFT{0<)w)x_sioJB0J1k4{F>`mixPP6<u7VL[w`q.J8!USF[|3TskduPl+6Oxh3f');
define('SECURE_AUTH_SALT', 'h}5%;2r.wS)So<7U.Z1ebH/z#3AwD*ZgQ=ZL>&JYMWG3:<o:gUnZi;p!By-E,2ss');
define('LOGGED_IN_SALT',   ';}0~CifXgKAIl1_r+5wfdHWq&C?_WFh5$8O{!s6OkvuXU,v[UC~pO]+xlJek-ml@');
define('NONCE_SALT',       '96CaY%ZDdOD.n(hfYa%fO<x)tD=3f%ACvb//J7:h/-zy|{^ DnvI&NcB1L3!XN2m');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_pallabiT_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
